function Extend(){
    var box = document.getElementById("box3");
    var width = 100;
    var animation = setInterval(animation, 1);

    if(box.classList.contains("clicked")){
        clearInterval(animation);
        return;
    }

    else{
        box.classList.add("clicked");
    }

    function animation() {
        if(width == 300) {
          clearInterval(animation);
        } else {

          width++; 
          box.style.width = width + "px";
        }
    }
}